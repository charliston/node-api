var express = require('express');
var router = express.Router();
var _ = require('lodash');

var version = '/v1';

router.route(version +'/processos')

	// // create 
	// .post(function(req, res) {
	// 		res.status(201).send();
	// })

	// get all the items
	.get(function(req, res) {
		// CARREGA A LISTA
		var lista = require('../json/processos/depositos/list.json');

		// PARAMETROS DE LISTAGEM
		var count = parseInt(req.query.count);
		var page = parseInt(req.query.page);
		var busca = req.query.busca;

		// NORMALIZA A BUSCA P/ UPPERCASE
		var buscaUpper = [];
		_.each(busca, function(n, k){
			buscaUpper[k] = n.toUpperCase();
		});

		// FILTRA A LISTA
		lista = (_.where(lista, buscaUpper));
		var total = lista.length;

		// PAGINACAO
		lista = _.slice(lista, (page-1)*count, page*count);

		res.json({
			"processos": lista,
			"meta": {
				"total": total,
				"pages": parseInt(Math.ceil(total / count))
			}
		});
	});
	router.route(version +'/processos/:id')
	// get the item with that id
	.get(function(req, res) {
		var id = req.params.id;
		res.json(require('../json/processos/depositos/' + id + '.json'));
	});

	// // update the item with this id
	// .put(function(req, res) {
	// 		var id = req.params.id;
	// 		res.status(200).send();
	// })

	// // delete the item with this id
	// .delete(function(req, res) {
	// 		var id = req.params.id;
	//		res.status(204).send();
	// });

module.exports = router;