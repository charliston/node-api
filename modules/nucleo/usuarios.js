var express = require('express');
var router = express.Router();
var _ = require('lodash');

router.route('/')
    // create
    .post(function(req, res) {
        res.status(201).send();
    })

    // get all the items
    .get(function(req, res) {
        // CARREGA A LISTA
        var lista = require('../../json/nucleo/usuarios/lista.json');

        // PARAMETROS DE LISTAGEM
        var count = parseInt(req.query.count) || 10;
        var page = parseInt(req.query.page) || 1;
        var busca = req.query.busca;

        /*
        // NORMALIZA A BUSCA P/ UPPERCASE
        var buscaUpper = [];
        _.each(busca, function(n, k){
            buscaUpper[k] = n.toUpperCase();
        });

        // FILTRA A LISTA
        lista = (_.where(lista, buscaUpper));
*/
        var total = lista.length;

        // PAGINACAO
        lista = _.slice(lista, (page-1)*count, page*count);

        res.json({
            "usuarios": lista,
            "meta": {
                "total": total,
                "pages": parseInt(Math.ceil(total / count))
            }
        });
    });

router.route('/:id')
    // get the item with that id
    .get(function(req, res) {
        var id = req.params.id;
        res.json(require('../../json/nucleo/usuarios/ver.json'));
    })

    // update the item with this id
    .put(function(req, res) {
        var id = req.params.id;
        res.status(200).send();
    })

    // delete the item with this id
    .delete(function(req, res) {
        var id = req.params.id;
        res.status(204).send();
    });


module.exports = router;