var express = require('express');
var router = express.Router();
var _ = require('lodash');

var indices = {
    "indices": [
        {
            "indice": "tipo_esfera",
            "texto": "Tipo esfera"
        },
        {
            "indice": "tipo_natureza",
            "texto": "Tipo natureza"
        },
        {
            "indice": "tipo_poder",
            "texto": "Tipo poder"
        }
    ]
};

router.route('/')
    // create
    .post(function(req, res) {
        res.status(201).send();
    })

    // get all the items
    .get(function(req, res) {
        if(req.query.tipos) {
            var lista_final = {},
                tipos = req.query.tipos.split(','),
                lista = '';
            _.forEach(tipos, function(i, n) {
                lista = '';
                try {
                    lista = require('../../json/nucleo/enumeracoes/'+ i +'.json');
                } catch (err) {}
                if(lista != '') {
                    lista_final[i] = lista;
                }
            });
            res.json({
                "enumeracoes": lista_final
            });
        } else {
            res.json(indices);
        }
    });

module.exports = router;