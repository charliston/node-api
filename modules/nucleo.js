var express = require('express');
var router = express.Router();
var app = express();
var _ = require('lodash');

var admin = {usuario:{id:1,nome:"admin",username:"admin",email:"admin@doccenter.com.br",admin:!0,authentication_token:"72241c3742d973ea5949ffd5ae11a866",modulos:[],ultimo_acesso:"2015-10-27T12:20:51.881Z",unidade_atual:{id:1,nome:"Bradoc",sigla:null,cnpj:"03.193.166/0001-60",tipo_posicao_id:1,orgao_superior_id:null,orgao_superior:{id:null,nome:null},entidade_id:1,entidade:{id:1,nome:"Prixx"}},permissoes:{},cliente_id:null}};
var user = {"usuario":{"id":6,"nome":"user","username":"user","email":"user@user.s","admin":true,"authentication_token":"a986d5a00828c559d83782c322e3cb36","modulos":[],"ultimo_acesso":"2015-11-05T16:54:51.960Z","unidade_atual":{"id":1,"nome":"Bradoc","sigla":null,"cnpj":"03.193.166/0001-60","tipo_posicao_id":1,"orgao_superior_id":null,"orgao_superior":{"id":null,"nome":null},"entidade_id":1,"entidade":{"id":1,"nome":"Prixx"}},"permissoes":{},"cliente_id":1}};

var version = '/v1';

/*
* USUARIOS
* */
router.route(version +'/usuarios/me')
.get(function(req, res) {
	res.json(user);
});

router.route(version +'/logar')
.post(function(req, res) {
	var body = req.body,
	username = body.username,
	password = body.password;
	if (username == 'admin' && password == 'admin') {
		res.status(201).json(admin);
	} else if (username == 'user' && password == 'admin') {
		res.status(201).json(user);
	}else {
		res.send({
			success: false,
			message: 'Invalid username/password'
		});
	}	
});

router.route(version +'/desconectar')
.delete(function(req, res) {
	res.status(204).send();
});


router.get(version +'/usuarios/me/unidades', function(req, res) {
  res.json({
    unidades: {}
  });
});

router.get(version +'/auditorias', function(req, res) {
  res.json({
    auditorias: {},
    meta: {
      total: 0,
      pages: 0
    }
  });
});

module.exports = router;