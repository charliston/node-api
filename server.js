// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');

// configure app
app.use(morgan('dev')); // log requests to the console

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 3000; // set our port

// MIDDLEWARE CORS
// =============================================================================
app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

	// intercept OPTIONS method
	if ('OPTIONS' == req.method) {
		res.send(200);
	}
	else {
		next();
	}
});

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});


// on routes that end in /nucleo
// ----------------------------------------------------
var nucleo = require('./modules/nucleo');
app.use('/nucleo', nucleo);

// VERSAO DO NUCLEO
var nucleo_v = 'v1';

// USUARIOS
app.use('/nucleo/'+ nucleo_v +'/usuarios', require('./modules/nucleo/usuarios'));

// CLIENTES
app.use('/nucleo/'+ nucleo_v +'/clientes', require('./modules/nucleo/clientes'));

// ENTIDADES
app.use('/nucleo/'+ nucleo_v +'/entidades', require('./modules/nucleo/entidades'));

// ENUMERACOES
app.use('/nucleo/'+ nucleo_v +'/enumeracoes', require('./modules/nucleo/enumeracoes'));

// PERMISSOES
app.use('/nucleo/'+ nucleo_v +'/permissoes', require('./modules/nucleo/permissoes'));


// on routes that end in /depositos
// ----------------------------------------------------
var depositos = require('./modules/depositos');
app.use('/depositos', depositos);

// REGISTER OUR ROUTES -------------------------------
app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Listening on port ' + port);
